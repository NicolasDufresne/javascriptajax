function objetAjax() {
    var ajax = null;

    if (window.XMLHttpRequest) {
        ajax = new XMLHttpRequest();
        return ajax;
    }

    else {
        alert('Installe un vrai navigateur');
        return false;
    }
}